package edu.uab.l3li3l.pkg.app;

/**
 * 
 * @author Daniel Latham "l3li3l@uab.edu"
 * Nov 7, 2014
 * 8:02:02 PM
 *
 * @param <E> for which E extends number and is the same type E in the user-implemented operations
 * object
 */
public class FractionOperations<E extends Number> implements Arithmetic<Fraction<E>> {

	private IntegerArithmetic<E> prim;
	/**
	 * Constructs new FractionOperations with given primitive operation implementation
	 * Note: Type safe
	 * @param prim Implemented IntegerArithmetic class
	 */
	public FractionOperations(IntegerArithmetic<E> prim) {
		this.prim = prim;
	}


	/**
	 * Simplifies the fraction by getting the greatest common denominator and dividing both
	 * the numerator and the denominator by that, and then instantiating a new Fraction
	 * with those created values
	 * @param num
	 * @param denom
	 * @return new Fraction that has both the numerator and denominator 
	 */
	private Fraction<E> simplify(E num, E denom)
	{
		try
		{
			E gcd = prim.gcd(num, denom);
			E nom_simpl = null;
			E denom_simpl = null;
			nom_simpl = prim.divide( num, gcd);
			denom_simpl = prim.divide(denom, gcd);
			return new Fraction<E>( nom_simpl,  denom_simpl);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Fraction<E> add(Fraction<E> lhs, Fraction<E> rhs) {
		try
		{
			E nom = null;
			E denom = null;
			nom =  prim.add(  prim.multiply(  lhs.getNumerator(),  rhs.getDenominator())
					, prim.multiply(  rhs.getNumerator(),  lhs.getDenominator())
					);
			denom = prim.multiply(  lhs.getDenominator(),  rhs.getDenominator());
			return simplify(nom, denom);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Fraction<E> subtract(Fraction<E> lhs, Fraction<E> rhs) {
		try
		{
			E nom = null;
			E denom = null;
			nom = prim.subtract(prim.multiply(lhs.getNumerator(), rhs.getDenominator())
					, prim.multiply( rhs.getNumerator(), lhs.getDenominator())
					);
			denom =  prim.multiply( lhs.getDenominator(), rhs.getDenominator());

			return simplify(nom, denom);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Fraction<E> multiply(Fraction<E> lhs, Fraction<E> rhs) {
		E num = prim.multiply(lhs.getNumerator(), rhs.getNumerator());
		E denom = prim.multiply(lhs.getDenominator(), rhs.getDenominator());
		return simplify(num, denom);
		
	}

	/**
	 * {@inheritDoc}
	 */
	public Fraction<E> divide(Fraction<E> lhs, Fraction<E> rhs) {
		try
		{
			E nom = null;
			E denom = null;
			
			nom = prim.multiply(lhs.getNumerator(), rhs.getDenominator());
			denom = prim.multiply( lhs.getDenominator(),  rhs.getNumerator());

			return simplify(nom, denom);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Fraction<E> negate(Fraction<E> operand) {
		try
		{
			E nom = null;
			E denom = null;
			nom =  prim.negate( operand.getNumerator());
			denom =   operand.getDenominator();
			return new Fraction<E>(nom, denom);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public Fraction<E> one() {
		try
		{
			
			E num = prim.one();
			E denom = prim.one();
			
			return new Fraction<E>(num , denom);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
	}

	/**
	 * {@inheritDoc}
	 */
	public Fraction<E> zero() {
		try
		{
			E num = prim.zero();
			E denom = prim.one();
			
			return new Fraction<E>(num , denom);
		} catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
}
