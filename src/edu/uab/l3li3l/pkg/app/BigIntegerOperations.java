package edu.uab.l3li3l.pkg.app;

import java.math.BigInteger;
/**
 * {@inheritDoc}
 *
 */
public class BigIntegerOperations implements IntegerArithmetic<BigInteger> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger mod(BigInteger dividend, BigInteger denominator) {
		return dividend.mod(denominator);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger gcd(BigInteger num, BigInteger denom) {
		return num.gcd(denom);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger divide(BigInteger dividend, BigInteger divisor) {
		return dividend.divide(divisor);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger multiply(BigInteger left, BigInteger right) {
		return left.multiply(right);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger add(BigInteger left, BigInteger right) {
		return  left.add(right);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger subtract(BigInteger left, BigInteger right) {
		return  left.subtract(right);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger negate(BigInteger operand) {
		return operand.negate();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger one()
	{
		return new BigInteger("1");
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public BigInteger zero()
	{
		return new BigInteger("0");
	}

}
