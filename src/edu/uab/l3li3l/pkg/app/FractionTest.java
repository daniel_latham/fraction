package edu.uab.l3li3l.pkg.app;

/**
 * JUnit assertions imported as static classes
 */
import static org.junit.Assert.*;

import org.junit.Test;


import java.math.BigInteger;

/**
 * 
 * Test using the JUnit Framework v. 4
 * JUnit should be included with this project, but if it is not,
 * it is easily accessible through google search 
 * No assertion errors are displayed when running this test, therefore,
 * it must work for all other tests. Logic: Weak Induction
 * @author Daniel Latham "l3li3l@uab.edu"
 * Nov 7, 2014
 * 8:12:40 PM
 *
 */
public class FractionTest {

	/**
	 * First test with the BigInteger for BigInteger type Fractions
	 * @return void
	 * @precondition java version 1.5 or greater is used
	 */
	@Test
	public void testBigInteger() {
		//System.out.println(Math.pow(.1, 2));
		Double str0 = 16.0;
		Double str1 = 4.0;
		Double str2 = 16.0;
		Double str3 = 4.0;
		BigInteger a = new BigInteger(str0.toString().split("\\.")[0]);
		BigInteger b = new BigInteger(str1.toString().split("\\.")[0]);
		BigInteger c = new BigInteger(str2.toString().split("\\.")[0]);
		BigInteger d = new BigInteger(str3.toString().split("\\.")[0]);
		Fraction<BigInteger> f1 = new Fraction<BigInteger>(a,b);
		Fraction<BigInteger> f2 = new Fraction<BigInteger>(c,d);
		FractionOperations<BigInteger> e = new FractionOperations<BigInteger>(new BigIntegerOperations());
		
		assertEquals("Add failed", "8".hashCode(), e.add(f1,f2).toString().hashCode(), 0);
		assertEquals("Sub failed", "0".hashCode(), e.subtract(f1,f2).toString().hashCode(), 0);
		assertEquals("Mul failed", "16".hashCode(), e.multiply(f1,f2).toString().hashCode(), 0);
		assertEquals("Div failed", "1".hashCode(), e.divide(f1,f2).toString().hashCode(), 0);
		assertEquals("Negate failed", "(-16 / 4)".hashCode(), e.negate(f1).toString().hashCode(), 0);
	}

	/**
	 * Test for Integer type Fractions
	 * @precondition Java version 1.5 or greater is used
	 */
	@Test
	public void testInteger() {
		Double str0 = -25.0;
		Double str1 = 1.0;
		Double str2 = 66.0;
		Double str3 = 3.0;
		Integer a = new Integer(str0.toString().split("\\.")[0]);
		Integer b = new Integer(str1.toString().split("\\.")[0]);
		Integer c = new Integer(str2.toString().split("\\.")[0]);
		Integer d = new Integer(str3.toString().split("\\.")[0]);
		Fraction<Integer> f1 = new Fraction<Integer>(a,b);
		Fraction<Integer> f2 = new Fraction<Integer>(c,d);
		FractionOperations<Integer> e = new FractionOperations<Integer>(new IntegerOperations());
		
		assertEquals("Add failed", "-3".hashCode(), e.add(f1,f2).toString().hashCode(), 0);
		assertEquals("Sub failed", "-47".hashCode(), e.subtract(f1,f2).toString().hashCode(), 0);
		assertEquals("Mul failed", "-550".hashCode(), e.multiply(f1,f2).toString().hashCode(), 0);
		assertEquals("Div failed", "(-25 / 22)".hashCode(), e.divide(f1,f2).toString().hashCode(), 0);
		assertEquals("Negate failed", "25".hashCode(), e.negate(f1).toString().hashCode(), 0);
	}

}
