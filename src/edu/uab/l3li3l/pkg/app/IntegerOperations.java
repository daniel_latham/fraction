package edu.uab.l3li3l.pkg.app;
/**
 * 
 * @author Daniel Latham "l3li3l@uab.edu"
 * Nov 7, 2014
 * 7:43:50 PM
 *
 */
public class IntegerOperations implements IntegerArithmetic<Integer> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer mod(Integer dividend, Integer denominator) {
		return dividend%denominator;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer gcd(Integer num, Integer denom){
		Integer a = Math.abs(num);
		Integer b = Math.abs(denom);
		
		if(b==0) return a;
		else return (gcd(b, a%b));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer divide(Integer dividend, Integer divisor) {
		return dividend/divisor;
	}

	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer multiply(Integer left, Integer right) {
		return left*right;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer add(Integer left, Integer right) {
		return left+right;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer subtract(Integer left, Integer right) {
		return left-right;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer negate(Integer operand) {
		return -operand;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer one() {
		return 1;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer zero() {
		return 0;
	}

}
