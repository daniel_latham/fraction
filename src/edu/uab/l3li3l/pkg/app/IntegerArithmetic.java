package edu.uab.l3li3l.pkg.app;
/**
 * Extended interface for the Integral Arithmetic operations since in my
 * Arithmetic interface it is implemented by my FractionOperations class, and
 * thus does not need the mod and gcd functionality, making this project object-oriented 
 * and type safe
 * @author Daniel Latham "l3li3l@uab.edu"
 * Nov 7, 2014
 * 7:42:39 PM
 *
 * @param <N> where N extends Number and can have math done onto it
 */
public interface IntegerArithmetic<N extends Number> extends Arithmetic<N> {
	/**
	 * Computes and returns the dividend mod denominator
	 * @param dividend The top of the equation
	 * @param denominator The bottom of the equation
	 * @return Generic dividend divided by denominator
	 */
	N mod(N dividend, N denominator);
	/**
	 * Computes and returns the greatest common denominator of the numerator and denominator
	 * @param num The top of the Fraction<N>
	 * @param denom The bottom of the Fraction<N>
	 * @return The computed GCD 
	 */
	N gcd(N num, N denom);

}
