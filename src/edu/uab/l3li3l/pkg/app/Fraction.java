package edu.uab.l3li3l.pkg.app;

/**
 * 
 * My implementation of a Fraction object that holds the values of an integral
 * numerator and denominator
 * @author Daniel Latham "l3li3l@uab.edu"
 * Nov 7, 2014
 * 8:12:58 PM
 *
 * @param <E> such that E extends Number and can have math done onto it
 */
@SuppressWarnings("serial")
public class Fraction<E extends Number> extends Number {

	
	private E numerator;
	private E denominator;
	/**
	 * 
	 * @param numerator
	 * @param denominator
	 * @precondition denominator must not be negative for certain operations(BigInteger errors), 
	 * so it's best to steer clear of it altogether
	 * @precondition denominator must not be zero
	 */
	public Fraction(E numerator, E denominator) {
		assert (!denominator.toString().equals("0")): "Denominator can not be zero";
		this.numerator = numerator;
		this.denominator = denominator;
		
	}

	  /**
	   * returns a String representation for the Fraction
	   */
	  public String toString()
	  {
		// do not print Fraction, if denominator is 1
		if ("1".equals(denominator.toString())) return numerator.toString();

		return "(" + numerator + " / " + denominator + ")";
	  }

	  /* Methods required by Number */

	  /**
	   * calculates double value of this Fraction
	   */
	  public double doubleValue()
	  {
		
		Double n = new Double(numerator.toString());
		Double d = new Double(denominator.toString());
		return n/d;
	  }

	  /**
	   * calculates float value of this Fraction
	   */
	  public float floatValue() { return (float)doubleValue(); }

	  /**
	   * calculates long value of this Fraction
	   */
	  public long  longValue()  { return (long)doubleValue(); }

	  /**
	   * calculates int value of this Fraction
	   */
	  public int   intValue()   { return (int)doubleValue(); }

	  /* equals and hashCode */

	  /**
	   * equals returns true, if numerator and denominator are equal
	   *
	   * note, this is not numeric equality
	   */
	  public boolean equals(Object o)
	  {
		if (o == null) return false;
		if (getClass() != o.getClass()) return false;

		@SuppressWarnings("unchecked")
		Fraction<E> that = (Fraction<E>) o;
		return (  this.getNumerator() == that.getNumerator()
			   && this.getDenominator() == that.getDenominator()
			   );
	  }

	  /**
	   * hashCode implementation in line with equals
	   */
	  public int hashCode()
	  {
		return this.getNumerator().hashCode() ^ this.getDenominator().hashCode();
	  }

	  /* getter methods */

	  /**
	   * returns the numerator
	   */
	  public E getNumerator()   { return numerator; }

	  /**
	   * returns the denominator
	   */
	  public E getDenominator() { return denominator; }

}
