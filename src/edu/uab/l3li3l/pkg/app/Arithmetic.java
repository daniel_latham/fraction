package edu.uab.l3li3l.pkg.app;
/**
 * Defines all of the primitive arithmetic specific to the data type used @see java.lang.Number
 * @author Daniel Latham "l3li3l@uab.edu"
 * Nov 7, 2014
 * 7:42:18 PM
 *
 * @param <N> where N extends Number and can have math done on it
 */
public interface Arithmetic<N extends Number>
{
	/**
	 * Divides two Numbers and returns the result
	 * @param dividend Generic type top of division
	 * @param divisor Generic type bottom of division
	 * @return Generic type result of the division dividend/divisor
	 */
	N divide(N dividend, N divisor);
	/**
	 * Multiplies two Numbers and returns the result
	 * @param left Lefthand side of the equation
	 * @param right Righthand side of the equation
	 * @return Generic type result of the multiplication of the left and right-hand sides
	 */
	N multiply(N left, N right);
	/**
	 * Adds two Numbers and returns the result
	 * @param left Lefthand side of the equation
	 * @param right Righhand side of the equation
	 * @return Generic type result of the addition of the left and right-hand sides
	 */
	N add(N left, N right);
	/**
	 * Subtracts two Numbers and returns the result
	 * @param left Lefthand side of the equation
	 * @param right Righthand side of the equation
	 * @return Generic type result of the addition of the left and righthand sides
	 */
	N subtract(N left, N right);
	/**
	 * Negates the generic type operand
	 * @param operand  The Number to negate
	 * @return The operand  negated (+ to -, - to +, 0 stays 0)
	 */
	N negate(N operand);
	
	/**
	 * Returns One of this generic type
	 * returns a representation for the value 1
	 * @return Positive value 1 in wrapper
	 * @postcondition must return the value positive 1
	 */
	N one();

	/**
	 * Returns a representation for the value 0
	 *
	 * Note, this is for convenience only, because zero() == one() - one()
	 * @return Zero or 0 or an object representing 0
	 * @postcondition must returns the value 0
	 */
	N zero();

}

